// AlgoLab3.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h" 
#include "math.h" 
#include "stdlib.h" 
#include <time.h> 

short freqCheck(short array[], int num);

int main() {
	unsigned int  a = 1103515245, c = 12345, m = 1<<31, len = 5000, i;
	unsigned int *values;
	values = (unsigned int*)malloc(sizeof(unsigned int)*len);
	values[0] = time(NULL);
	for (i = 0; i < len - 1; i++)
		values[i + 1] = (a*values[i] + c) % m;
	short *newValues;
	newValues = (short*)malloc(sizeof(short)*len);
	for (i = 0; i < len; i++)
		newValues[i] = (float)values[i] / (m - 1) * 100;
	short freq[100];
	printf("\nFrequency of intervals of occurrence of random variables:\n"); 
	for (i = 0; i < 100; i++)
	{
		freq[i] = freqCheck(newValues, i);
		printf("Value #%d = %d\n", i + 1, freq[i]);
	}
	double staticPrbbl[100];
	printf("\nStatistical probability of occurrence of random variables:\n"); 
	for (i = 0; i < 100; i++)
	{
		staticPrbbl[i] = ((double)freq[i] / len);
		printf("Value #%d = %.5f\n", i, staticPrbbl[i]);
	}
	double mathExp = 0;
	for (i = 0; i < 100; i++)
		mathExp += i * staticPrbbl[i];
	printf("\nMathematical expectation of random variables: %.6f\n", mathExp); 
	double dyspersion = 0;
	for (i = 0; i < 100; i++)
		dyspersion += pow((i - mathExp), 2) * staticPrbbl[i];
	printf("\nDispersion of random variables: %f\n", dyspersion); 
	printf("\nMedian deviation of random variables: %f\n", sqrt(dyspersion)); 
	return 0;
}

short freqCheck(short array[], int num) 
{
	short freq = 0;
	for (int i = 0; i < 5000; i++)
		if (array[i] == num)
			freq++;
	return freq;
}

