﻿using System;
namespace Graphs
{
    class Program
    {
        static bool[] visit = new bool[19];
        static string[] city = new string[19]
        {
            "Киев",
            "Житомир",
            "Новоград-Волынский",
            "Ровно",
            "Луцк",
            "Бердичев",
            "Винница",
            "Хмельницкий",
            "Тернополь",
            "Шепетовка",
            "Белая церковь",
            "Черкасы",
            "Кременчуг",
            "Умань",
            "Полтава",
            "Харьков",
            "Прилуки",
            "Сумы",
            "Миргород"
        };
        static int[,] way = new int[19, 19];
        static void Main(string[] args)
        {
            int k = 0;
            dfs(k, " ", 0);
            Console.ReadKey();
        }
        public static void dfs(int k, string s, int res)
        {
            string strWay = s + " >>>>> " + city[k];
            if (res != 0)
                strWay += " (" + res + ")";
            way[0, 1] = 135;
            way[1, 2] = 80;
            way[2, 3] = 100;
            way[3, 4] = 68;
            way[1, 5] = 38;
            way[5, 6] = 73;
            way[6, 7] = 110;
            way[7, 8] = 104;
            way[1, 9] = 115;
            way[0, 10] = 78;
            way[10, 11] = 146;
            way[11, 12] = 105;
            way[10, 13] = 115;
            way[10, 14] = 181;
            way[14, 15] = 130;
            way[0, 16] = 128;
            way[16, 17] = 175;
            way[16, 18] = 109;
            visit[k] = true;
            Console.WriteLine(strWay);
            for (int i = 0; i < 19; i++)
            {
                if (visit[i] == false && way[k, i] != 0)
                    dfs(i, strWay, res += way[k, i]);
            }
        }
    }
}
