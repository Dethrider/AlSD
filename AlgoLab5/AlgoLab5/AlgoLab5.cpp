// AlgoLab5.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include <windows.h>
#include <time.h>

void fill_array(int n, int* arr);
void sort_one(int n, int* mass);
void sort_two(int n, int* arr);
void sort_three(int n, int* arr);
void sort_four(int n, int* arr);

int main()
{
	SetConsoleCP(1251);  // ��������� ������� �������� win-cp 1251 � ����� �����
	SetConsoleOutputCP(1251); // ��������� ������� �������� win-cp 1251 � ����� ������
	srand(time(NULL));
	int loops = 100000, amount = 1000;  // ���������� ������ ���������� // ���������� ��������� �������   
	int *arr1 = new int[amount], *arr2 = new int[amount], *arr3 = new int[amount], *arr4 = new int[amount];  // ������ ��� �������� ����������� ������

																											

	printf("���������� ���������.\n");
	time_t point1_1 = time(NULL);
	for (int i = 0; i < loops; i++) {
		fill_array(amount, arr1);
	}
	time_t point2_1 = time(NULL);
	for (int i = 0; i < loops; i++) {
		fill_array(amount, arr1);
		sort_one(amount, arr1);
	}
	time_t point3_1 = time(NULL);

	printf("P1 %d\n", point1_1);
	printf("P2 %d\n", point2_1);
	printf("P3 %d\n", point3_1);
	double duration1 = (double)((point3_1 - point2_1) - (point2_1 - point1_1)) / loops;
	printf("����� ����������: %f\n", duration1);

	

	printf("���������� ���������.\n");
	time_t point1_2 = time(NULL);
	for (int i = 0; i < loops; i++) {
		fill_array(amount, arr2);
	}
	time_t point2_2 = time(NULL);
	for (int i = 0; i < loops; i++) {
		fill_array(amount, arr2);
		sort_two(amount, arr2);
	}
	time_t point3_2 = time(NULL);

	printf("P1 %d\n", point1_2);
	printf("P2 %d\n", point2_2);
	printf("P3 %d\n", point3_2);
	double duration2 = (double)((point3_2 - point2_2) - (point2_2 - point1_2)) / loops;
	printf("����� ����������: %f\n", duration2);

	

	printf("���������� �������.\n");
	time_t point1_3 = time(NULL);
	for (int i = 0; i < loops; i++) {
		fill_array(amount, arr3);
	}
	time_t point2_3 = time(NULL);
	for (int i = 0; i < loops; i++) {
		fill_array(amount, arr3);
		sort_three(amount, arr3);
	}
	time_t point3_3 = time(NULL);

	printf("P1 %d\n", point1_3);
	printf("P2 %d\n", point2_3);
	printf("P3 %d\n", point3_3);
	double duration3 = (double)((point3_3 - point2_3) - (point2_3 - point1_3)) / loops;
	printf("����� ����������: %f\n", duration3);

	

	printf("���������� �����.\n");
	time_t point1_4 = time(NULL);
	for (int i = 0; i < loops; i++) {
		fill_array(amount, arr4);
	}
	time_t point2_4 = time(NULL);
	for (int i = 0; i < loops; i++) {
		fill_array(amount, arr4);
		sort_four(amount, arr4);
	}
	time_t point3_4 = time(NULL);

	printf("P1 %d\n", point1_4);
	printf("P2 %d\n", point2_4);
	printf("P3 %d\n", point3_4);
	double duration4 = (double)((point3_4 - point2_4) - (point2_4 - point1_4)) / loops;
	printf("����� ����������: %f\n", duration4);

	return 0;
}

void fill_array(int n, int* arr)
{
	int a = -1000, b = 1000;
	for (int i = 0; i < n; i++)
		arr[i] = a + rand() % (b - a + 1);
}

void sort_one(int n, int* arr)
{
	int fl, tmp;
	do
	{
		fl = 0;
		for (int i = 0; i < n; i++)
		{
			if (arr[i - 1] > arr[i])
			{
				tmp = arr[i];
				arr[i] = arr[i - 1];
				arr[i - 1] = tmp;
				fl = 1;
			}
		}
	} while (fl);
}

void sort_two(int n, int* arr)
{
	int tmp;
	for (int i = 0; i < n; i++)
	{
		tmp = arr[i];
		for (int j = i - 1; j >= 0 && arr[j] > tmp; j--)
		{
			arr[j + 1] = arr[j];
			arr[j] = tmp;
		}
	}
}

void sort_three(int n, int* arr)
{
	int mini, tmp;
	for (int i = 0; i < n - 1; i++)
	{
		mini = i;
		for (int j = i + 1; j < n; j++)
		{
			if (arr[j] < arr[mini])
				mini = j;
			tmp = arr[i];
			arr[i] = arr[mini];
			arr[mini] = tmp;
		}
	}
}

void sort_four(int n, int* arr)
{
	int step = n / 2, tmp;
	while (step > 0)
	{
		for (int i = 0; i < (n - step); i++)
		{
			int j = i;
			while (j >= 0 && arr[j] > arr[j + step])
			{
				int tmp = arr[j];
				arr[j] = arr[j + step];
				arr[j + step] = tmp;
				j--;
			}
		}
		step /= 2;
	}
}

