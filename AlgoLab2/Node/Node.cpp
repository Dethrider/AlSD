// Node.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include "stdio.h"
#include "stdlib.h"

typedef int elemtype;

struct elem {
	elemtype value;        
	struct elem* next;      
	struct elem* prev;      
};

struct myList {
	struct elem* head;    
	struct elem* tail;      
	size_t size;            
};

typedef struct elem cNode;
typedef struct myList cList;

cList* createList(void);
void deleteList(cList* list);
bool isEmptyList(cList* list);
int pushFront(cList* list, elemtype* data);
int popFront(cList* list, elemtype* data);
int pushBack(cList* list, elemtype* data);
int popBack(cList* list, elemtype* data);
cNode* getNode(cList* list, int index);
void printList(cList* list, void(*fun)(elemtype*));
void printNode(elemtype* value);


int main()
{
	/*elemtype a = 0;
	elemtype b = 1;
	elemtype c = 2;
	elemtype tmp;

	cList* mylist = createList();

	pushFront(mylist, &a);
	printList(mylist, printNode);
	printf("\n");

	popBack(mylist, &tmp);

	pushFront(mylist, &b);
	printList(mylist, printNode);
	printf("\n");

	pushFront(mylist, &a);
	printList(mylist, printNode);
	printf("\n");

	pushBack(mylist, &c);
	printList(mylist, printNode);
	printf("\n");

	popFront(mylist, &tmp);
	printList(mylist, printNode);
	printf("\n");

	pushBack(mylist, &a);
	printList(mylist, printNode);
	printf("\n");

	pushFront(mylist, &a);
	printList(mylist, printNode);
	printf("\n");

	printNode(&getNode(mylist, 2)->value);
	printf("\n");

	printList(mylist, printNode);
	printf("\n");

	deleteList(mylist);*/
	char choice;
	cList* mylist;
	mylist = createList();
	do {
		elemtype tmp;
		printf("1. Add element to list;\n2. Delete element from list;\n3. Show list;\n4. Delete list;\n0. Quit\n > ");
		scanf_s("%hu", &choice);
		switch (choice)
		{
		case 1:
		{
			printf("Your value: "); scanf_s("%d", &tmp);
			pushFront(mylist, &tmp);
			break;
		}
		case 2:
		{
			popBack(mylist, &tmp);
			break;
		}
		case 3:
		{
			printf("Your list: \n");
			printList(mylist, printNode);
			break;
		}
		case 4:
		{
			deleteList(mylist);
			printf("Your list is clear!\n");
			break;
		}
		default:
			printf("Error404. Enter another value!\n");
			break;
		}
	} while (choice != 0);
	return 0;
}

cList* createList(void) {
	cList* list = (cList*)malloc(sizeof(cList));
	if (list) {
		list->size = 0;
		list->head = list->tail = NULL;
	}
	return list;
}


void deleteList(cList *list) {
	cNode* head = list->head;
	cNode* next = NULL;
	while (head) {
		next = head->next;
		free(head);
		head = next;
	}
	free(list);
	list = NULL;
}

bool isEmptyList(cList* list) {
	return ((list->head == NULL) || (list->tail == NULL));
}

int pushFront(cList* list, elemtype* data) {
	cNode* node = (cNode*)malloc(sizeof(cNode));
	if (!node) {
		return(-1);
	}
	node->value = *data;
	node->next = list->head;
	node->prev = NULL;

	if (!isEmptyList(list)) {
		list->head->prev = node;
	}
	else {
		list->tail = node;
	}
	list->head = node;

	list->size++;
	return(0);
}


int popFront(cList* list, elemtype* data) {
	cNode* node;

	if (isEmptyList(list)) {
		return(-2);
	}

	node = list->head;
	list->head = list->head->next;

	if (!isEmptyList(list)) {
		list->head->prev = NULL;
	}
	else {
		list->tail = NULL;
	}

	*data = node->value;
	list->size--;
	free(node);

	return(0);
}


int pushBack(cList* list, elemtype* data) {
	cNode* node = (cNode*)malloc(sizeof(cNode));
	if (!node) {
		return(-3);
	}

	node->value = *data;
	node->next = NULL;
	node->prev = list->tail;
	if (!isEmptyList(list)) {
		list->tail->next = node;
	}
	else {
		list->head = node;
	}
	list->tail = node;

	list->size++;
	return(0);
}


int popBack(cList* list, elemtype* data) {
	cNode *node = NULL;

	if (isEmptyList(list)) {
		return(-4);
	}

	node = list->tail;
	list->tail = list->tail->prev;
	if (!isEmptyList(list)) {
		list->tail->next = NULL;
	}
	else {
		list->head = NULL;
	}

	*data = node->value;
	list->size--;
	free(node);

	return(0);
}


cNode* getNode(cList* list, int index) {
	cNode *node = NULL;
	int i;

	if (index >= list->size) {
		return (NULL);
	}

	if (index < list->size / 2) {
		i = 0;
		node = list->head;
		while (node && i < index) {
			node = node->next;
			i++;
		}
	}
	else {
		i = list->size - 1;
		node = list->tail;
		while (node && i > index) {
			node = node->prev;
			i--;
		}
	}

	return node;
}


void printList(cList* list, void(*func)(elemtype*)) {
	cNode* node = list->head;

	if (isEmptyList(list)) {
		return;
	}

	while (node) {
		func(&node->value);
		node = node->next;
	}
}

void printNode(elemtype* value) {
	printf("%d\n", *((int*)value));
}

