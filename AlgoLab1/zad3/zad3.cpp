// zad3.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"



int main()
{
	signed char fst;
	fst = 5 + 127;
	printf("1st:\t%d / %X\n", fst, fst);
	signed char snd;
	snd = 2 - 3;
	printf("2nd:\t%d / %X\n", snd, snd);
	signed char trd;
	trd = -120 - 34;
	printf("3rd:\t%d / %X\n", trd, trd);
	signed char frth;
	frth = (unsigned)(-5);
	printf("4th:\t%d / %X\n", frth, frth);
	signed char fth;
	fth = 56 & 38;
	printf("5th:\t%d / %X\n", fth, fth);
	signed char sth;
	sth = 56 | 38;
	printf("6th:\t%d / %X\n", sth, sth);
	return 0;
}

