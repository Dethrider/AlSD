// AlgoLab1.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"

struct Time
{
	unsigned short day : 5;
	unsigned short month : 4;
	unsigned short year : 7;
	unsigned short hour : 5;
	unsigned short minute : 6;
	unsigned short seconds : 6;
};

int main()
{
	unsigned short arr = 0;
	Time n;
	printf("Enter day: "); scanf_s("%hu", &arr); n.day = arr;
	printf("Enter month: "); scanf_s("%hu", &arr); n.month = arr;
	printf("Enter year: "); scanf_s("%hu", &arr); n.year = arr;
	printf("Enter hour: "); scanf_s("%hu", &arr); n.hour = arr;
	printf("Enter minutes: "); scanf_s("%hu", &arr); n.minute = arr;
	printf("Enter seconds: "); scanf_s("%hu", &arr); n.seconds = arr;
	int memory = sizeof(Time);
	printf("! - - - Memory in bytes: %d - - - !\n", memory);
	printf("Date: %hu.%hu.%hu\nTime: %hu:%hu:%hu\n", n.day, n.month, n.year, n.hour, n.minute, n.seconds);
	return 0;
}


