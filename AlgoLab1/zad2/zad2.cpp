// zad2.cpp: определяет точку входа для консольного приложения.
//
#include "stdafx.h"

union db
{
	struct
	{
		signed short n1 : 1;
		signed short n2 : 1;
		signed short n3 : 1;
		signed short n4 : 1;
		signed short n5 : 1;
		signed short n6 : 1;
		signed short n7 : 1;
		signed short n8 : 1;
		signed short n9 : 1;
		signed short n10 : 1;
		signed short n11 : 1;
		signed short n12 : 1;
		signed short n13 : 1;
		signed short n14 : 1;
		signed short n15 : 1;
		signed short n16 : 1;
	}type;
	signed short dataPointer;
};

int main()
{
	db pointer;
	signed short value;
	printf("Enter your value: "); scanf_s("%hu", &value);
	pointer.dataPointer = value;
	int sign;
	if ((pointer.type.n16 & 1) == 0)
		sign = 0;
	else
		sign = 1;
	printf("0 - positive value\n1 - negative value\nYou entered: %d\nSign: %d\n", pointer.dataPointer, sign);
	return 0;
}

