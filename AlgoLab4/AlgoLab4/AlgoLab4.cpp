////// AlgoLab4.cpp: ���������� ����� ����� ��� ����������� ����������.
//////
////
#include "stdafx.h"
#include "math.h"
#include "stdlib.h"
#include "locale"
#include "time.h"
#define  n 0.001f
#define MAX_LEN 4700

float multiplication(float tmp)
{
  return	tmp * tmp;
}
float Rect(float first, float end)
{
float saveN = 0.0;
float st = n;
for (float i = 0; i < end; i += st)
{
	saveN += multiplication(i);
}
saveN *= st;
return saveN;
}

float Trapetion(float first, float end)
{
	float saveN = 0.0;
	float st = n;
	for (float i = 0; i < end; i+= st)
	{
		saveN += 2 * multiplication(i);
	}
	saveN = (saveN + multiplication(first) + multiplication(end)) * st / 2;
	return saveN;
}

float Simpson(float first, float end)
{
	int x = 1;
	float st = n, integr = 0, integr1 = 0, integr2 = 0;
	for (float i = 0; i < end; i += st)
	{
		if (x % 2 == 0)
		{
			integr1 += multiplication(i);
		}
		else
			integr2 += multiplication(i);
		x++;
	}
	integr = (integr + multiplication(first) + multiplication(end) + 4 * integr2 + 2 * integr1)* st / 3;
	return integr;
}
double MonteCarlo(double first, double end)
{
	double a, b, x, y, sum, integVal = 0;
	int step = end - first, nums = 0;

	if (multiplication(first) > multiplication(end))
	{
		b = multiplication(first);
		a = end - first;
	}
	else
	{
		b = multiplication(end);
		a = end - first;
	}
	sum = a * b;

	for (double i = first; i < end; i += n)
	{
		x = (rand()*1.0 / RAND_MAX)*step + first;
		y = (rand()*1.0 / RAND_MAX) * (b);
		if (y <= multiplication(x))
		{
			nums++;
		}
	}
	integVal = sum * (((double)nums) / MAX_LEN);
	return integVal / 2;
}

int main()
{
	setlocale(LC_ALL, "Rus");
	int first = 1, end = 10;
	printf("��������� ��������: %d\n", first);
	printf("�������� ��������: %d\n", end);
	printf("����� ��������������: %f\n", Rect(first, end));
	printf("����� ��������: %f\n", Trapetion(first, end));
	printf("����� ��������: %f\n", Simpson(first, end));
	printf("����� �����-�����: %lf\n", MonteCarlo(first, end));
    return 0;
}
